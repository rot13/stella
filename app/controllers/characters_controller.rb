class CharactersController < ApplicationController
	before_action :set_character, only: [:show, :edit, :update, :destroy]


	def index
    	@characters = Character.all
  	end

  	def show
  	end

  	def new
  		@character = Character.new
  	end

  	def edit
  	end

	def create
		# @user = User.find_by_id(2)
		@user = current_user
		@character = @user.characters.create(character_params)
		redirect_to character_path(@character)
	end


	def update
		if @character.update(character_params)
			redirect_to @character, notice: 'Character was successfully updated.'
		else
			render action: 'edit'
		end
	end

	def destroy
		@character.destroy
		redirect_to characters_url
	end

	private

		def set_character
	      @character = Character.find(params[:id])
	    end


		def character_params
			params.require(:character).permit(:name,:surname)
			#params.require(:character).permit(current_user.name, :surname)
		end
end
