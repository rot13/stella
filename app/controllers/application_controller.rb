class ApplicationController < ActionController::Base
  include Pundit
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  # Able to see only the main page without the character
  before_action :have_character , except: [:index]
  
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def user_not_authorized
    flash[:alert] = "Access denied."
    redirect_to (request.referrer || root_path)
  end

  def have_character
    #debug current_user
    if user_signed_in?
      if action_name!='destroy'
        if controller_name!= 'characters' && controller_name!='devise/sessions'
          if current_user.characters.blank?
            #current_user.inspect
            redirect_to new_character_path
          end
        end
      end
    end
  end

end
